#ifndef _BHTTP_VERSION_H
#define _BHTTP_VERSION_H

#include <stdexcept>
#include <string>

namespace BHTTP
{
  enum class Version
  {
    V_10,
    V_11,
    V_20
  };

  Version StringToVersion (const std::string&) throw (std::invalid_argument);
  std::string VersionToString (const Version) noexcept;
}

#endif
