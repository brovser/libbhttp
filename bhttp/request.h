#ifndef _BHTTP_REQUEST_H
#define _BHTTP_REQUEST_H

#include <bhttp/method.h>
#include <bhttp/version.h>

#include <istream>
#include <map>
#include <ostream>
#include <string>
#include <sstream>

namespace BHTTP
{
  class Request : public std::stringstream
  {
  public:
    Request () noexcept;
    Request (const Request&) noexcept;
    Request (Method, const std::string&, Version) noexcept;
    Request (Method, const std::string&, Version,
             const std::map<std::string, std::string>&) noexcept;
    Request (Method, const std::string&, Version,
             const std::map<std::string, std::string>&, const std::string&)
      noexcept;
    virtual ~Request () noexcept;

    void PutDefaultHeaders () noexcept;

    Method method;
    std::string resource;
    Version version;
    std::map<std::string, std::string> headers;
  };
}

std::istream& operator>> (std::istream&, BHTTP::Request&);
std::ostream& operator<< (std::istream&, const BHTTP::Request&);

#endif
