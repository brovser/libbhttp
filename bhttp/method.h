#ifndef _BHTTP_METHOD_H
#define _BHTTP_METHOD_H

#include <stdexcept>
#include <string>

namespace BHTTP
{
  enum class Method
  {
    GET,
    HEAD,
    PUT,
    POST,
    DELETE,
    OPTIONS,
    TRACE,
    CONNECT,
    PATCH
  };

  Method StringToMethod (const std::string&) throw (std::invalid_argument);
  std::string MethodToString (const Method) noexcept;
}

#endif
