/*
 * This file is compiled first to ensure that all BHTTP headers are
 * self-integral, and prevent compiling the parts which may cause errors.
 */

#include <bhttp.h>

// Strict ISO C++ says we cannot have empty units. This trick is very simple:
static int dummy = 1;
