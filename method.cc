#include <bhttp/method.h>

namespace BHTTP
{
  Method
  StringToMethod (const std::string& str) throw (std::invalid_argument)
  {
#define METH(n) if (str == #n) return Method::n
#include "methods.h"
#undef METH
    throw std::invalid_argument(std::string{"Invalid HTTP method string: "} +
                                str);
  }

  std::string
  MethodToString (const Method meth) noexcept
  {
    switch (meth)
    {
#define METH(n) case Method::n: return #n
#include "methods.h"
#undef METH
    default:
      // This should not happen if we are compiling with -fstrict-enums
      return "";
    }
  }
}
