#include <config.h>

#include <bhttp/request.h>

#include <regex>

using std::size_t;

namespace BHTTP
{
  Request::Request () noexcept {}
  Request::Request (const Request& req) noexcept :
    std::stringstream(req.str ()), method (req.method),
    resource (req.resource), version (req.version), headers (req.headers) {}
  Request::Request (Method meth, const std::string& res, Version ver) noexcept
    : method (meth), resource (res), version (ver) {}
  Request::Request (Method meth, const std::string& res, Version ver,
                    const std::map<std::string, std::string>& hdrs) noexcept
    : method (meth), resource (res), version (ver), headers (hdrs) {}
  Request::Request (Method meth, const std::string& res, Version ver,
                    const std::map<std::string, std::string>& hdrs,
                    const std::string& cont) noexcept
    : std::stringstream (cont), method (meth), resource (res), version (ver),
    headers (hdrs) {}
  Request::~Request () noexcept
  {
  }

  void
  Request::PutDefaultHeaders () noexcept
  {
    // This notifies the server of the version we are using (TODO: this may be
    // a Git revision number for unstable builds), so that if this server
    // supports BHTTP specific features, it may use one. For example, if FOO
    // method was added in 1.13, this may help the server determine whether it
    // may use it.
    if (headers.count ("X-BHTTP") == 0)
      headers["X-BHTTP"] = PACKAGE_VERSION;

    // If a server encounters a bug in request, which did not mean tampering
    // with the request by user, the server may log this address.
    if (headers.count ("X-BHTTP-BugReport") == 0)
      headers["X-BHTTP-BugReport"] = PACKAGE_BUGREPORT;
  }
}

static bool regexesInit = false;
static std::regex startRegex, headerlineRegex;

static void
init_regex (void)
{
  if (regexesInit) return;
  regexesInit = true;

  startRegex =
    std::regex("^((GET|HEAD|PUT|POST|DELETE|OPTIONS|TRACE|CONNECT|PATCH)) ([^ ]+) ((HTTP\\/1\\.0|HTTP\\/1\\.1|HTTP\\/2\\.0))$",
               std::regex_constants::optimize |
               std::regex_constants::ECMAScript);
  headerlineRegex = std::regex("^([^:]+):(.*)$",
                               std::regex_constants::optimize |
                               std::regex_constants::ECMAScript);
}

std::istream&
operator>> (std::istream& stream, BHTTP::Request& req)
{
  if (stream.eof ())
    throw std::invalid_argument (std::string(__func__) + ": EOF before the "
                                 "header line");
  std::string linebuf;
  std::getline (stream, linebuf);
  std::smatch m;
  if (!std::regex_match (linebuf, m, startRegex))
    throw std::invalid_argument (std::string (__func__) + ": Invalid start "
                                 "line: " + linebuf);
  req.method = BHTTP::StringToMethod (m[1]);
  req.resource = m[2];
  req.version = BHTTP::StringToVersion (m[3]);
  while (1)
    {
      if (stream.eof ())
        throw std::invalid_argument (std::string (__func__) + ": EOF before "
                                     "the empty line ending headers");
      std::getline (stream, linebuf);
      if (linebuf == "" || linebuf == "\r")
        {
          break;
        }
      if (!std::regex_match (linebuf, m, headerlineRegex))
        throw std::invalid_argument (std::string (__func__) + ": Invalid "
                                     "header line: " + linebuf);
      req.headers [m[1]] = m[2];
    }
  req.str ("");
  if (req.headers.count ("TE"))
    {
      if (req.headers["TE"] == "chunked")
        {
          if (!req.headers.count ("Content-Length"))
            {
              throw std::invalid_argument (std::string (__func__) +
                                           ": TE is \"chunked\", but no "
                                           "Content-Length");
            }
          size_t nbytes = std::strtoull (req.headers["TE"].c_str (),
                                         nullptr, 10);
          while (nbytes-- > 0)
            {
              req.put (stream.get ());
            }
        }
      else if (req.headers["TE"] == "identity")
        {
          while (stream)
            {
              req.put (stream.get ());
            }
        }
      else if (req.headers["TE"] == "gzip" || req.headers["TE"] == "compress"
               || req.headers ["TE"] == "deflate")
        {
          throw std::invalid_argument (std::string (__func__) + ": Content "
                                       "compression not yet supported!");
        }
      else
        {
          throw std::invalid_argument (std::string (__func__) + ": Illegal "
                                       "TE value: " + req.headers ["TE"]);
        }
    }
  return stream;
}
