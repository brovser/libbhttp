#include <bhttp/version.h>

namespace BHTTP
{
  Version
  StringToVersion (const std::string& str) throw (std::invalid_argument)
  {
#define VERS(n) if (str == #n) return Version::n
#include "versions.h"
#undef VERS
    throw std::invalid_argument(std::string{"Invalid HTTP version string: "} +
                                str);
  }

  std::string
  VersionToString (const Version vers) noexcept
  {
    switch (vers)
    {
#define VERS(n) case Version::n: return #n
#include "versions.h"
#undef VERS
    default:
      // This should not happen if we are compiling with -fstrict-enums
      return "";
    }
  }
}
